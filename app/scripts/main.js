"use strict";

console.log('Welcome to EventsManager');

// Print console log to html element:

(function () {
    var old = console.log;
    var logger = document.getElementById('log');
    console.log = function () {
        var message = Array.prototype.slice.call(arguments, 0);
        old.apply(this, message);
        if (typeof message == 'object') {
            var m = message.join(' ');
            logger.innerHTML += '<div style="margin:5px;border:1px solid black;padding:4px;">' + m + '</div>'; //(JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br />';
        } else {
            logger.innerHTML += message + '<br />';
        }
    }
})();

var EventsManager = function () {

    var subscriptions = {};

    var on = function (eventName, callback, context) {
        if (!(eventName in subscriptions)) {
            subscriptions[eventName] = [];
        }
        if (typeof(callback) == "string") {
            callback = context[callback];
        }
        subscriptions[eventName].push({
            context: context,
            callback: callback
        });
    };

    var off = function (eventName, callback) {
        if (!(eventName in subscriptions) || !subscriptions[eventName].length) return;
        subscriptions[eventName] = $.map(subscriptions[eventName], function (subscription) {
                if (subscription.callback == callback) {
                    return null;
                } else {
                    return(subscription);
                }
            }
        );
    };

    var trigger = function (eventName, data) {
        var event = {
            type: eventName,
            data: (data || []),
            result: null
        };

        var eventArguments = event.data;
        if (subscriptions[eventName]) {
            subscriptions[eventName].forEach(function (subscription) {
                return subscription.callback.apply(subscription.context, [eventArguments]);
            });
        }
        return event;
    };

    return {
        on: on,
        off: off,
        trigger: trigger
    };
};


var MyEventsManager = new EventsManager();

/*
 NOTICE:After you add your code of EventsManager you should run
 all the code and test your success with the code below.
 Meaning, the code below should work without any errors
 */

var Person = function (name, eventManager) {
    this.name = name;
    this.foods = [];
    this.em = eventManager;
};

Person.prototype.waitToEat = function () {
    this.em.on('breakfast:ready', this.eat, this);
};

Person.prototype.eat = function (foods) {
    console.log("i'm", this.name, "and i'm eating", foods.join(","));
    this.foods.length = 0;
    this.foods = foods;
    this.em.trigger('eat:done', this);
};

Person.prototype.finishEat = function (time) {
    console.log("i'm", this.name, "and i finished eating at", time);
    this.em.off("breakfast:ready", this.finishEat);
};

Person.prototype.logFood = function () {

    /* solution #1:*/

    var that = this;
    that.foods.forEach(function (item) {
        console.log("/* solution #1:*/ I'm " + that.name + " and I ate" + item);
    });


    //    solution #2:
    this.foods.forEach(function (item) {
        console.log("/* solution #2:*/ I'm " + this.name + " and I ate " + item);
    }, this);


};

//starttheapp

MyEventsManager.on('eat:done', function (person) {
    console.log(person.name, "finished eating");
});

MyEventsManager.on('breakfast:ready', function (menu) {
    console.log("breakfast is ready with:", menu);
});

var john = new Person('john', MyEventsManager);

john.waitToEat();

MyEventsManager.on('eat:done', function (person) {
    person.finishEat(new Date());
});

var breakfast = ["scrambledeggs", "tomatoes", "bread", "butter"];

MyEventsManager.trigger('breakfast:ready', breakfast);

/*
 CHALLENGE2: Please FIX the source code of "logFood" according to the instructions:
 this "logFood" method throws an error.
 "this.name" doesn't print the Person's name
 Please suggest 2 different solutions (by adding the relevant fix code)
 so "this.name" will print the relevant name
 */

john.logFood();